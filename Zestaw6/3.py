import unittest
from rectangle import *

class TestRectangle(unittest.TestCase):
    def setUp(self):
        self.rect = Rectangle(1, 2, 4, 8)

    def test_str(self):
        self.assertEqual(str(self.rect), "[(1, 2), (4, 8)]")

    def test_repr(self):
        self.assertEqual(repr(self.rect), "Rectangle(1, 2, 4, 8)")
    
    def test_eq(self):
        self.assertEqual(self.rect, Rectangle(1, 2, 4, 8))
        
    def test_ne(self):
        self.assertNotEqual(self.rect, Rectangle(1, 2, 4, 7))
        self.assertNotEqual(self.rect, Rectangle(1, 1, 4, 8))
    
    def test_center(self):
        self.assertEqual(self.rect.center(), Point(2.5, 5))
     
    def test_area(self):
        self.assertEqual(self.rect.area(), 18)
    
    def test_move(self):
        self.rect.move(1, 1)
        self.assertEqual(self.rect, Rectangle(2, 3, 5, 9))

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()
