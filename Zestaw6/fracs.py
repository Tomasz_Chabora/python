class Frac:
    def __init__(self, x=0, y=1):
        if y == 0:
            raise ZeroDivisionError("denominator can't be 0")
        
        self.x = x
        self.y = y
        self._reduce()
    
    def __str__(self):
        self._reduce()
        if self.y == 1:
            return str(self.x)
        else:
            return "{}/{}".format(self.x, self.y)
    
    def __repr__(self):
        self._reduce()
        return "Frac({}, {})".format(self.x, self.y)
        
    def __add__(self, other):
        if self.y != other.y:
            self.x *= other.y
            other.x *= self.y
            self.y = other.y = self.y * other.y
        
        return Frac(self.x + other.x, self.y)

    def __sub__(self, other):
        if self.y != other.y:
            self.x *= other.y
            other.x *= self.y
            self.y = other.y = self.y * other.y
        
        return Frac(self.x - other.x, self.y)

    def __mul__(self, other):
        return Frac(self.x * other.x, self.y * other.y)

    def __div__(self, other):
        return Frac(self.x * other.y, self.y * other.x)

    def __pos__(self):
        return self

    def __neg__(self):
        return Frac(-self.x, self.y)

    def __invert__(self):
        return Frac(self.y, self.x)

    def __cmp__(self, other):
        f1 = float(self)
        f2 = float(other)
        
        if f1 == f2:
            return 0
        elif f1 < f2:
            return -1
        else:
            return 1

    def __float__(self):
        return float(self.x) / float(self.y)
    
    def _reduce(self):
        a = self.x
        b = self.y
        
        while b != 0:
            c = a % b
            a = b
            b = c
        
        self.x /= a
        self.y /= a
