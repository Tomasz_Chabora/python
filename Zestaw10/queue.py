class Queue:
    def __init__(self, max_size=-1):
        self.items = []
        self.max_size = max_size

    def __str__(self):
        return str(self.items)

    def is_empty(self):
        return not self.items

    def is_full(self):
        return (len(self.items) == self.max_size)

    def put(self, data):
        if self.is_full():
            raise RuntimeError("Full queue")
        self.items.append(data)

    def get(self):
        if self.is_empty():
            raise RuntimeError("Empty queue")
        return self.items.pop(0)
