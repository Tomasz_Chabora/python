class Stack:
    def __init__(self, max_size=-1):
        self.items = []
        self.max_size = max_size

    def __str__(self):
        return str(self.items)

    def is_empty(self):
        return not self.items

    def is_full(self):
        return (len(self.items) == self.max_size)

    def push(self, item):
        if self.is_full():
            raise RuntimeError("Full stack")
        self.items.append(item)

    def pop(self):
        if self.is_empty():
            raise RuntimeError("Empty stack")
        return self.items.pop()
