from random import randint

class RandomQueue:
    class QueueNode:
        def __init__(self, element):
            self.element = element
            self.next = self.prev = None
        
        def __str__(self):
            return "[{}]".format(self.element)
    
    def __init__(self, max_size):
        self.head = None
        self.tail = None
        self.begin = None
        self.middle = None
        self.end = None
        
        self.size = 0
        self.max_size = max_size

    def is_empty(self):
        return self.size == 0

    def is_full(self):
        return self.size == self.max_size

    def insert(self, data):
        if self.is_full():
            raise RuntimeError("Full queue")
        
        node = self.QueueNode(data)
        if not self.head:
            self.head = node
        if self.tail:
            self.tail.next = node
            node.prev = self.tail
        self.tail = node
        
        self.size += 1
        self._refresh_pointers()

    def remove(self):
        if self.is_empty():
            raise RuntimeError("Empty queue")
        
        start = ["self.begin", "self.middle", "self.end"][randint(0, 2)]
        dir = randint(0, 1)
        move = randint(0, 3)
        
        for i in range(move):
            if dir == 0 and eval("{}.prev != None".format(start)):
                exec("{} = {}.prev".format(start, start))
            elif dir == 1 and eval("{}.next != None".format(start)):
                exec("{} = {}.next".format(start, start))
        
        cur = eval(start)
        
        if cur == self.head:
            self.head = self.head.next
        elif cur == self.tail:
            self.tail = self.tail.prev
        
        if cur.prev:
            cur.prev.next = cur.next
        if cur.next:
            cur.next.prev = cur.prev
        
        self.size -= 1
        self._element_removed(cur)
        return cur.element
    
    def _refresh_pointers(self):
        if not self.begin:
            self.begin = self.head
        
        if not self.end:
            self.end = self.tail
        elif self.end.next:
            self.end = self.end.next
    
        if not self.middle:
            self.middle = self.head
        elif self.middle.next and self.size % 2 == 0:
            self.middle = self.middle.next
    
    def _element_removed(self, element):
        for str in ["self.begin", "self.middle", "self.end"]:
            if eval("{}.next".format(str)):
                exec("{} = {}.next".format(str, str))
            elif eval("{}.prev".format(str)):
                exec("{} = {}.prev".format(str, str))
            else:
                exec("{} = None".format(str))
        self._refresh_pointers()
