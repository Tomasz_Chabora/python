import unittest
from stack import *

class TestStack(unittest.TestCase):
    def setUp(self):
        self.stack = Stack(3)
    
    def test_raise(self):
        with self.assertRaises(RuntimeError):
            self.stack.pop()
        
        self.stack.push(1)
        self.stack.push(2)
        self.stack.push(3)
        with self.assertRaises(RuntimeError):
            self.stack.push(4)
            
    def test_push_pop(self):
        self.stack.push(1)
        self.assertEqual(str(self.stack), "[1]")
        self.stack.push(2)
        self.assertEqual(str(self.stack), "[1, 2]")
        self.stack.pop()
        self.assertEqual(str(self.stack), "[1]")
        self.assertEqual(self.stack.pop(), 1)
            
    def test_empty_full(self):
        self.assertTrue(self.stack.is_empty())
        self.assertFalse(self.stack.is_full())
        self.stack.push(1)
        self.assertFalse(self.stack.is_empty())
        self.assertFalse(self.stack.is_full())
        self.stack.push(2)
        self.stack.push(3)
        self.assertFalse(self.stack.is_empty())
        self.assertTrue(self.stack.is_full())

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()
