import datetime
from random import randint

from randomqueue import *

def test_time(queue):
    t0 = datetime.datetime.now()
    
    for i in range(10000):
        if i%2 == 0:
            queue.insert(randint(0, 1000))
        else:
            element = queue.remove()
    
    print("Execution time: {}\n".format(datetime.datetime.now() - t0))

rq = RandomQueue(10001)

for i in range(100):
    for ii in range(100):
        rq.insert(i * 100 + ii)
    print("Testing for {} elements...".format((i+1) * 100))
    test_time(rq)