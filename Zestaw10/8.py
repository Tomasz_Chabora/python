from randomqueue import *
LIST_SIZE = 10

def insert(queue, value):
    queue.insert(value)
    print("{} inserted".format(value))

rq = RandomQueue(100)
s = set()

print("Filling queue...")

for i in range(LIST_SIZE):
    rq.insert(i)
    print("{} inserted".format(i))
    
print("Retrieving elements...")

for i in range(LIST_SIZE):
    el = rq.remove()
    print("{} removed".format(el))
    s.add(el)

print("Success!" if len(s) == LIST_SIZE else "Failure...")