import unittest
from queue import *

class TestQueue(unittest.TestCase):
    def setUp(self):
        self.queue = Queue(3)
    
    def test_raise(self):
        with self.assertRaises(RuntimeError):
            self.queue.get()
        
        self.queue.put(1)
        self.queue.put(2)
        self.queue.put(3)
        with self.assertRaises(RuntimeError):
            self.queue.put(4)
            
    def test_put_get(self):
        self.queue.put(1)
        self.assertEqual(str(self.queue), "[1]")
        self.queue.put(2)
        self.assertEqual(str(self.queue), "[1, 2]")
        self.queue.get()
        self.assertEqual(str(self.queue), "[2]")
        self.assertEqual(self.queue.get(), 2)
            
    def test_empty_full(self):
        self.assertTrue(self.queue.is_empty())
        self.assertFalse(self.queue.is_full())
        self.queue.put(1)
        self.assertFalse(self.queue.is_empty())
        self.assertFalse(self.queue.is_full())
        self.queue.put(2)
        self.queue.put(3)
        self.assertFalse(self.queue.is_empty())
        self.assertTrue(self.queue.is_full())

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()
