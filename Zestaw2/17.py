line = "some random text with multiple words and ."
words = line.split()

sort1 = sorted(words)
sort2 = sorted(words, key=len)

print("Sorted words: " + ", ".join(sort1))
print("Sorted by length: " + ", ".join(sort2))