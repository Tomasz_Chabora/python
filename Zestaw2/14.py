line = "some random text with multiple words"
words = line.split()
longest_word = max(words, key=len)

print('line = "' + line + '"')
print("Longest word: " + longest_word)
print("Longest word length: " + str(len(longest_word)))