line = "some random text with multiple words"
words = line.split()

string1 = ""
string2 = ""
for word in words:
    string1 += word[0]
    string2 += word[-1]

print('line = "' + line + '"')
print('First letters = "' + string1 + '"')
print('Last letters = "' + string2 + '"')