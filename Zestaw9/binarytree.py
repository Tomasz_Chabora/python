class TreeNode:
    def __init__(self, number):
        self.left = self.right = None
        self.number = number

def add_left_child(node, number):
    node.left = TreeNode(number)
    return node.left
    
def add_right_child(node, number):
    node.right = TreeNode(number)
    return node.right

def count_leafs(root):
    stack = [root]
    count = 0
    
    while stack:
        node = stack.pop()
        if (not node.left) and (not node.right):
            count += 1
        else:
            if node.left:
                stack.append(node.left)
                
            if node.right:
                stack.append(node.right)
    
    return count

def count_total(root):
    stack = [root]
    count = 0
    
    while stack:
        node = stack.pop()
        count += node.number
        
        if node.left:
            stack.append(node.left)
            
        if node.right:
            stack.append(node.right)
    
    return count
