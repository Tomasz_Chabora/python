class SingleList:
    class Node:
        def __init__(self, data, next=None):
            self.data = data
            self.next = next
    
    def __init__(self):
        self.head = self.Node(None)
        self.tail = self.head
    
    def __repr__(self):
        rep = "SingleList("
        cur = self.head.next
        first = True
        
        while cur != None:
            rep += "{}{}".format("" if first else ", ", cur.data)
            cur = cur.next
            first = False
            
        return rep + ")"
    
    def push_back(self, data):
        self.tail.next = self.Node(data)
        self.tail = self.tail.next
    
    def reverse(self):
        f = self.head.next
        f2 = f.next

        while f2 != None:
            temp = f2.next
            f2.next = f

            f = f2
            f2 = temp

        self.tail = self.head.next
        self.tail.next = None
        self.head.next = f
