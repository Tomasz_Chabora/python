from singlelist import SingleList

l = SingleList()
l.push_back(1)
l.push_back(2)
l.push_back(3)
l.push_back(4)

print(repr(l))

l.reverse()

print(repr(l))
