from binarytree import *

root = TreeNode(1)
l = add_left_child(root, 1)
ll = add_left_child(l, 1)
llr =  add_right_child(ll, 1)
add_right_child(llr, 2)
add_left_child(llr, 3)
r = add_right_child(root, 1)
rr = add_right_child(r, 4)
rl = add_left_child(r, 1)
add_right_child(rl, 5)

print("Tree:\n     /1\\\n /1       /1\\\n1\\       1\\  4\n /1\\       5\n2   3\n")

print("Leafs: {}".format(count_leafs(root)))
print("Sum: {}".format(count_total(root)))
