while True:
    width = raw_input("Enter width: ")
    try:
        width = int(width)
        break
    except:
        print("Invalid input")
        
while True:
    height = raw_input("Enter height: ")
    try:
        height = int(height)
        break
    except:
        print("Invalid input")

grid = ""

for y in range(height):
    for x in range(width):
        grid += "+---"
    
    grid += "+\n"
    
    for x in range(width):
        grid += "|   "
        
    grid += "|\n"

for x in range(width):
    grid += "+---"
grid += "+"

print(grid)