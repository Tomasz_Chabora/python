from roman2int import roman2int

#sposob tworzenia 1
D1 = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
#sposob tworzenia 2
D2 = dict()
D2["I"] = 1
D2["V"] = 5
D2["X"] = 10
D2["L"] = 50
D2["C"] = 100
D2["D"] = 500
D2["M"] = 1000
#sposob tworzenia 3
D3 = dict([("I", 1), ("V", 5), ("X", 10), ("L", 50),
           ("C", 100), ("D", 500), ("M", 1000)])

print("I = " + str(D1["I"]))
print("V = " + str(D2["V"]))
print("X = " + str(D3["X"]))
print("L = " + str(D1["L"]))
print("C = " + str(D2["C"]))
print("D = " + str(D3["D"]))
print("M = " + str(D1["M"]))

while True:
    roman = raw_input("Enter Roman number: ")
    
    for char in roman:
        if char not in ["I", "V", "X", "L", "C", "D", "M"]:
            print ("Invalid input")
            break
    else:
        break

print(roman2int(roman))