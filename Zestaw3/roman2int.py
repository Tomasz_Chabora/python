def roman2int(roman):
    translator = {"I": 1, "IV": 4, "V": 5, "IX": 9, "X": 10,
    "XL": 40, "L": 50, "XC": 90, "C": 100, "CD": 400, "D": 500,
    "CM": 900, "M": 1000}
    
    result = 0
    roman = list(roman)
    
    while roman:
        if len(roman) > 1 and str(roman[0]) + str(roman[1]) in translator:
            result += translator[str(roman[0]) + str(roman[1])]
            roman.pop(0)
        else:
            result += translator[str(roman[0])]
            
        roman.pop(0)
    
    return result