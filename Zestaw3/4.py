stop = False

while not stop:
    number = raw_input("Enter number: ")
    try:
        number = float(number)
    except:
        if number == "stop":
            break
        else:
            print("Invalid input")
            continue
    print(str(number) + ", " + str(pow(number,3)))