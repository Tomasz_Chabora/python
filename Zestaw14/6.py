def degrees(graph):
	ret = {}
	for node, edges in graph.iteritems():
		ret[node] = len(edges)
	return ret

graph = {"A":["B","C"], "B":["A", "C","D"], "C":["A", "B", "D", "E"], "D":["B", "C"], "E":["C"], "F":[]}
print "Graph:\n{}".format(graph)

print "\nDegrees:\n{}".format(degrees(graph))