def list_nodes(graph):
	return graph.keys()

def list_edges(graph):
	ret = []
	for node, edges in graph.iteritems():
		for edge in edges:
			ret.append((node, edge))
	return ret

def count_nodes(graph):
	return len(graph.keys())

def count_edges(graph):
	i = 0
	for node, edges in graph.iteritems():
		i+=len(edges)
	return i

graph = {"A":["B","C"], "B":["C","D"], "C":["D"], "D":["C"], "E":["C"], "F":[]}
print "Graph:\n{}".format(graph)

print "\nNodes ({}):\n{}".format(count_nodes(graph), list_nodes(graph))

print "\nEdges ({}):\n{}".format(count_edges(graph), list_edges(graph))