import unittest
import math
from circle import *

class TestCircle(unittest.TestCase):
    def setUp(self):
        self.circ = Circle(0, 0, 4)
        
        with self.assertRaises(ValueError):
            Circle(0, 0, -4)

    def test_repr(self):
        self.assertEqual(repr(self.circ), "Circle(0, 0, 4)")
    
    def test_eq(self):
        self.assertEqual(self.circ, Circle(0, 0, 4))
        
        with self.assertRaises(ValueError):
            self.circ == Point(0, 0)
        
    def test_ne(self):
        self.assertNotEqual(self.circ, Circle(1, 1, 4))
        self.assertNotEqual(self.circ, Circle(0, 0, 5))
        
        with self.assertRaises(ValueError):
            self.circ != []
     
    def test_area(self):
        self.assertEqual(self.circ.area(), 16 * math.pi)
    
    def test_move(self):
        self.circ.move(1, 2)
        self.assertEqual(self.circ, Circle(1, 2, 4))
    
    def test_cover(self):
        self.assertEqual(self.circ.cover(Circle(8, 2 ,10)), Circle(7, 2, 11))
        
        with self.assertRaises(ValueError):
            self.circ.cover(Point(1,2))

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()
