class Frac:
    def __init__(self, x=0, y=1):
        if not isinstance(x, int):
            raise ValueError(
                "invalid argument type: {} (expecting int)".format(type(x)))
        elif not isinstance(y, int):
            raise ValueError(
                "invalid argument type: {} (expecting int)".format(type(y)))
            
        if y == 0:
            raise ZeroDivisionError("denominator can't be 0")
        
        self.x = int(x)
        self.y = int(y)
        self._reduce()
    
    def __str__(self):
        self._reduce()
        if self.y == 1:
            return str(self.x)
        else:
            return "{}/{}".format(self.x, self.y)
    
    def __repr__(self):
        self._reduce()
        return "Frac({}, {})".format(self.x, self.y)

    def __cmp__(self, other):
        f1 = float(self)
        f2 = float(other)
        
        if f1 == f2:
            return 0
        elif f1 < f2:
            return -1
        else:
            return 1
        
    def __add__(self, other):
        other = self._ensure_frac(other)
        x1 = self.x
        x2 = other.x
        y = self.y
        
        if self.y != other.y:
            x1 *= other.y
            x2 *= self.y
            y = self.y * other.y
        
        return Frac(x1 + x2, y)
    
    __radd__ = __add__

    def __sub__(self, other):
        other = self._ensure_frac(other)
        x1 = self.x
        x2 = other.x
        y = self.y
        
        if self.y != other.y:
            x1 *= other.y
            x2 *= self.y
            y = self.y * other.y
        
        return Frac(x1 - x2, y)
    
    def __rsub__(self, other):
        return -(self - other)

    def __mul__(self, other):
        other = self._ensure_frac(other)
        return Frac(self.x * other.x, self.y * other.y)
    
    __rmul__ = __mul__

    def __div__(self, other):
        other = self._ensure_frac(other)
        return Frac(self.x * other.y, self.y * other.x)
    
    def __rdiv__(self, other):
        return ~(self / other)

    def __pos__(self):
        return self

    def __neg__(self):
        return Frac(-self.x, self.y)

    def __invert__(self):
        return Frac(self.y, self.x)

    def __float__(self):
        return float(self.x) / float(self.y)
    
    def _reduce(self):
        a = self.x
        b = self.y
        
        while b != 0:
            c = a % b
            a = b
            b = c
        
        self.x /= a
        self.y /= a
    
    def _ensure_frac(self, other):
        if isinstance(other, Frac):
            return other
        elif isinstance(other, int):
            return Frac(other, 1)
        elif isinstance(other, float):
            return Frac(*float.as_integer_ratio(other))
        else:
            raise ValueError("invalid argument type: {}".format(type(other)))