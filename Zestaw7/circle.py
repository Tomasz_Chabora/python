import math

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def __eq__(self, other):
        return (self.x == other.x and self.y == other.y)

class Circle:
    def __init__(self, x=0, y=0, radius=1):
        if radius < 0:
            raise ValueError("negative radius")
        
        self.pt = Point(x, y)
        self.radius = radius

    def __repr__(self):
        return "Circle({}, {}, {})".format(self.pt.x, self.pt.y, self.radius)

    def __eq__(self, other):
        if other.__class__.__name__ != "Circle":
            raise ValueError("can't compare Circle with {}".format(
                            other.__class__.__name__))
        
        return self.pt == other.pt and self.radius == other.radius

    def __ne__(self, other):
        return not self == other

    def area(self):
        return math.pi * pow(self.radius, 2)

    def move(self, x, y):
        self.pt.x += x
        self.pt.y += y

    def cover(self, other):
        if other.__class__.__name__ != "Circle":
            raise ValueError("{} is not Circle".format(
                            other.__class__.__name__))
        
        minx = min(self.pt.x - self.radius, self.pt.x + self.radius,
                other.pt.x - other.radius, other.pt.x + other.radius)
        maxx = max(self.pt.x - self.radius, self.pt.x + self.radius,
                other.pt.x - other.radius, other.pt.x + other.radius)
        miny = min(self.pt.y - self.radius, self.pt.y + self.radius,
                other.pt.y - other.radius, other.pt.y + other.radius)
        maxy = max(self.pt.y - self.radius, self.pt.y + self.radius,
                other.pt.y - other.radius, other.pt.y + other.radius)
        
        return Circle((minx + maxx) / 2, (miny + maxy) / 2,
                max(abs(minx - maxx), abs(miny - maxy)) / 2)