import unittest
from fracs import *

class TestFrac(unittest.TestCase):
    def setUp(self): pass
    
    def test_init(self):
        with self.assertRaises(ZeroDivisionError):
            Frac(1, 0)
        
        with self.assertRaises(ValueError):
            Frac(1.0, 7)
        with self.assertRaises(ValueError):
            Frac(1, 0.5)
        with self.assertRaises(ValueError):
            Frac([], 3)
        with self.assertRaises(ValueError):
            Frac(6, ())
            
    def test_str(self):
        self.assertEqual(str(Frac(1, 3)), "1/3")
        self.assertEqual(str(Frac(2, 6)), "1/3")
        self.assertEqual(str(Frac(2, 1)), "2")
        self.assertEqual(str(Frac(6, 2)), "3")
        
    def test_repr(self):
        self.assertEqual(repr(Frac(1, 3)), "Frac(1, 3)")
        self.assertEqual(repr(Frac(2, 8)), "Frac(1, 4)")
    
    def test_cmp(self):
        self.assertEqual(cmp(Frac(1, 3), Frac(2, 3)), -1)
        self.assertEqual(cmp(Frac(1, 4), Frac(1, 3)), -1)
        self.assertEqual(cmp(Frac(1, 4), 1), -1)
        self.assertEqual(cmp(Frac(1, 2), Frac(1, 2)), 0)
        self.assertEqual(cmp(Frac(1, 2), Frac(2, 4)), 0)
        self.assertEqual(cmp(Frac(3, 1), 3), 0)
        self.assertEqual(cmp(Frac(2, 3), Frac(1, 3)), 1)
        self.assertEqual(cmp(Frac(1, 3), Frac(1, 4)), 1)
        self.assertEqual(cmp(Frac(5, 2), 2), 1)
        self.assertEqual(Frac(1, 2), Frac(2, 4))
        self.assertEqual(Frac(2, 1), 2)
        
    def test_add(self):
        self.assertEqual(Frac(1, 2) + Frac(1, 3), Frac(5, 6))
        self.assertEqual(Frac(1, 2) + 1, Frac(3, 2))
        self.assertEqual(2 + Frac(1, 3), Frac(7, 3))
        self.assertEqual(Frac(1, 4) + 0.25, Frac(1, 2))
        self.assertEqual(0.25 + Frac(1, 2), Frac(3, 4))
        
        with self.assertRaises(ValueError):
            Frac(1, 3) + []
        with self.assertRaises(ValueError):
            () + Frac(2, 4)

    def test_sub(self):
        self.assertEqual(Frac(1, 2) - Frac(1, 3), Frac(1, 6))
        self.assertEqual(Frac(3, 2) - 1, Frac(1, 2))
        self.assertEqual(2 - Frac(1, 3), Frac(5, 3))
        self.assertEqual(Frac(1, 4) - 0.25, Frac(0, 1))
        self.assertEqual(0.75 - Frac(1, 2), Frac(1, 4))
        
        with self.assertRaises(ValueError):
            Frac(1, 3) - []
        with self.assertRaises(ValueError):
            () - Frac(2, 4)

    def test_mul(self):
        self.assertEqual(Frac(1, 2) * Frac(3, 4), Frac(3, 8))
        self.assertEqual(Frac(1, 2) * 2, Frac(1, 1))
        self.assertEqual(2 * Frac(1, 3), Frac(2, 3))
        
        with self.assertRaises(ValueError):
            Frac(1, 3) * []
        with self.assertRaises(ValueError):
            () * Frac(2, 4)

    def test_div(self):
        self.assertEqual(Frac(1, 2) / Frac(3, 4), Frac(4, 6))
        self.assertEqual(Frac(1, 1) / 2, Frac(1, 2))
        self.assertEqual(3 / Frac(1, 2), 6)
        
        with self.assertRaises(ValueError):
            Frac(1, 3) / []
        with self.assertRaises(ValueError):
            () / Frac(2, 4)
        with self.assertRaises(ZeroDivisionError):
            Frac(2, 4) / 0

    def test_pos(self):
        self.assertEqual(+Frac(1, 2), Frac(1, 2))
        self.assertEqual(+Frac(1, 2), Frac(-1, -2))

    def test_neg(self):
        self.assertEqual(-Frac(1, 2), Frac(-1, 2))
        self.assertEqual(-Frac(1, 2), Frac(1, -2))
        self.assertEqual(-Frac(-1, 2), Frac(1, 2))
        self.assertEqual(-Frac(1, -2), Frac(-1, -2))

    def test_invert(self):
        self.assertEqual(~Frac(2, 3), Frac(3, 2))
        with self.assertRaises(ZeroDivisionError):
            ~Frac(0, 1)

    def test_float(self):
        self.assertEqual(float(Frac(1, 10)), 0.1)
        self.assertEqual(float(Frac(1, 4)), 0.25)
        self.assertEqual(float(Frac(1, 2)), 0.5)
        self.assertEqual(float(Frac(0, 1)), 0)
        self.assertEqual(float(Frac(0, 2)), 0)
        self.assertEqual(float(Frac(1, 1)), 1)
        self.assertEqual(float(Frac(2, 2)), 1)
        self.assertEqual(float(Frac(2, 1)), 2)
        self.assertEqual(float(Frac(8, 2)), 4)
        self.assertEqual(float(Frac(10, 4)), 2.5)

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()