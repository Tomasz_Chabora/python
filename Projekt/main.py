import pygame
from base_state import BaseState

class Window:
    """Main Pygame window."""
    def __init__(self, width=780, height=580):
        """Initialize window and create state."""
        pygame.init()
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption('Python Maze Generator')
        
        self.clock = pygame.time.Clock()
        self.state = BaseState(self)
    
    def show(self):
        """Main loop, calls update and draw until closed."""
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
                    
            self.clock.tick(60)
            self.update()
            self.screen.fill((0,0,0))
            self.draw()
            pygame.display.update()
    
    def update(self):
        """Updates state."""
        self.state.update()
    
    def draw(self):
        """Draws state."""
        self.state.draw(self.screen)

if __name__ == "__main__":
    window = Window()
    window.show()