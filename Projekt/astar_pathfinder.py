class AstarPathfinder:
    """Finds path between two points, using A* algorithm."""
    def __init__(self, space):
        """Loads space and evaluates it's dimensions."""
        self.space = space
        self.width = 0
        self.height = 0
        for square in space:
            if square[0] > self.width:
                self.width = square[0]
            if square[1] > self.height:
                self.height = square[1]
    
    def find_path(self, start, end):
        """Finds path from start to end."""
        open_list = {}
        closed_list = {}
        F_list = {}
        final_square = None
        
        open_list[start] = (0, "")
        
        while open_list:
            min_cost = float("infinity")
            chosen_sqare = None
            
            for square in open_list:
                if square == end:
                    final_square = square
                    break
                
                cost = self.get_cost(square, open_list[square], F_list, end)
                if cost < min_cost:
                    min_cost = cost
                    chosen_square = square
            
            if final_square:
                closed_list[final_square] = open_list[final_square]
                break
            
            if not chosen_square:
                raise RuntimeError("No path exists!")
            
            self.add_around_square(chosen_square, open_list, closed_list)
            closed_list[chosen_square] = open_list[chosen_square]
            del open_list[chosen_square]
        
        if not final_square:
            raise RuntimeError("No path exists!")
        
        path = []
        while final_square != start:
            path.append(final_square)
            if closed_list[final_square][1] == "u":
                final_square = (final_square[0], final_square[1]+1)
            elif closed_list[final_square][1] == "r":
                final_square = (final_square[0]+1, final_square[1])
            elif closed_list[final_square][1] == "d":
                final_square = (final_square[0], final_square[1]-1)
            elif closed_list[final_square][1] == "l":
                final_square = (final_square[0]-1, final_square[1])
        
        return path
    
    def add_around_square(self, square, open_list, closed_list):
        """Looks for square's neigbours to be added to open list."""
        sq = (square[0], square[1]-1)
        if square[1] > 0 and sq not in open_list and sq not in closed_list and\
            sq in self.space:
            open_list[sq] = (open_list[square][0]+1, "u")
        
        sq = (square[0]+1, square[1])
        if square[0] < self.width and sq not in open_list and\
            sq not in closed_list and sq in self.space:
            open_list[sq] = (open_list[square][0]+1, "l")
        
        sq = (square[0], square[1]+1)
        if square[1] < self.height and sq not in open_list and\
            sq not in closed_list and sq in self.space:
            open_list[sq] = (open_list[square][0]+1, "d")
            
        sq = (square[0]-1, square[1])
        if square[0] > 0 and sq not in open_list and sq not in closed_list and\
            sq in self.space:
            open_list[sq] = (open_list[square][0]+1, "r")
    
    def get_cost(self, square, data, F_list, end):
        """Returns square's cost, which is distance from start \
and distance to end."""
        if square not in F_list:
            F_list[square] = data[0] + abs(end[0] - square[0]) +\
                abs(end[1] - square[1])
        
        return F_list[square]