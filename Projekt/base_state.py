import pygame
from prim_generator import PrimGenerator
from astar_pathfinder import AstarPathfinder

class BaseState:
    """Base state, where maze is generated and draw."""
    TILE_WIDTH = 20
    TILE_HEIGHT = 20
    
    def __init__(self, screen):
        """Initialize state: generate maze and create path finder."""
        self.width = screen.width / self.TILE_WIDTH
        self.height = screen.height / self.TILE_HEIGHT
        self.end = self.start = self.path = None
        
        self.space = PrimGenerator().generate(self.width-1, self.height-1)
        self.pathfinder = AstarPathfinder(self.space)
        
    def update(self):
        """Update state: check where user clicks and find path."""
        tx, ty = int(pygame.mouse.get_pos()[0] / self.TILE_WIDTH),\
            int(pygame.mouse.get_pos()[1] / self.TILE_HEIGHT)
        
        if pygame.mouse.get_pressed()[0] and (tx, ty) in self.space:
            self.start = (tx, ty)
            self.path = self.end = None
        
        if self.start and (tx, ty) != self.start and (tx, ty) != self.end and\
            (tx, ty) in self.space:
            self.path = self.pathfinder.find_path(self.start, (tx, ty))
            self.end = (tx, ty)
        elif (tx, ty) == self.start:
            self.path = self.end = None
    
    def draw(self, screen):
        """Draws maze and path."""
        for x in range(self.width):
            for y in range(self.height):
                pygame.draw.rect(screen, (0,192,0)
                    if (x, y) not in self.space else (0,64,0),
                    (x * self.TILE_WIDTH, y * self.TILE_HEIGHT,
                    self.TILE_WIDTH, self.TILE_HEIGHT), 0)
        
        tx, ty = int(pygame.mouse.get_pos()[0] / self.TILE_WIDTH),\
            int(pygame.mouse.get_pos()[1] / self.TILE_HEIGHT)
        
        radius = min(self.TILE_WIDTH, self.TILE_HEIGHT)/2
        if (tx, ty) in self.space:
            pygame.draw.rect(screen, (255,0,0), (tx * self.TILE_WIDTH,
                ty * self.TILE_HEIGHT, self.TILE_WIDTH, self.TILE_HEIGHT), 2)
            
        if self.start:
            pygame.draw.circle(screen, (255,0,0),
                (self.start[0] * self.TILE_WIDTH + radius,
                self.start[1] * self.TILE_HEIGHT + radius), radius, 2)
        
        if self.path:
            for square in self.path:
                pygame.draw.circle(screen, (255,0,0),
                    (square[0] * self.TILE_WIDTH + radius,
                    square[1] * self.TILE_HEIGHT + radius), radius/2, 0)