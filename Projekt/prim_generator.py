from random import randint

class PrimGenerator:
    """Generates maze using Prim's algorithm."""
    def generate(self, width, height):
        """Here algorithm is executed and result space is returned."""
        tree = {}
        queue = []
        self.graph_width = width/2
        self.graph_height = height/2
        
        first = (randint(0, width/2-1), randint(0, height/2-1))
        tree[first] = (0, "")
        self.update_queue(first, tree, queue)
        
        while len(tree) < width*height/4:
            node, node2 = self.select_nearest(tree, queue)
            queue.remove(node2)
            tree[node2] = (tree[node][0]+1, self.get_dir(node, node2))
            self.update_queue(node2, tree, queue)
        
        space = []
        
        for node, data in tree.iteritems():
            space.append((node[0]*2+1, node[1]*2+1))
            if data[1] == "u":
                space.append((node[0]*2+1, node[1]*2))
            elif data[1] == "r":
                space.append((node[0]*2+2, node[1]*2+1))
            elif data[1] == "d":
                space.append((node[0]*2+1, node[1]*2+2))
            elif data[1] == "l":
                space.append((node[0]*2, node[1]*2+1))
        
        return space
    
    def update_queue(self, node, tree, queue):
        """Checks given node for neighbors to be added to queue."""
        for node2 in self.neighbours(node):
            if node2 not in queue and node2 not in tree:
                queue.append(node2)
    
    def neighbours(self, node):
        """Returns neighbours of a node."""
        neighs = []
        
        if node[0] > 0:
            neighs.append((node[0]-1, node[1]))
        if node[1] > 0:
            neighs.append((node[0], node[1]-1))
        if node[0] < self.graph_width-1:
            neighs.append((node[0]+1, node[1]))
        if node[1] < self.graph_height-1:
            neighs.append((node[0], node[1]+1))
        
        return neighs
    
    def select_nearest(self, tree, queue):
        """Searches for nearest reachable node in queue."""
        ret_node = (0, 0)
        ret_node2 = (0, 0)
        nearest = []
        dist = float('inf')
        
        for node, data in tree.iteritems():
            for node2 in self.neighbours(node):
                if node2 in queue  and node2 not in tree and data[0] <= dist:
                    if data[0] < dist:
                        dist = data[0]
                        nearest = []
                    nearest.append((node, node2))
        
        return nearest[randint(0, len(nearest)-1)]
    
    def get_dir(self, fro, to):
        """Returns string denoting direction from one point to another."""
        if fro[1] < to[1]:
            return "u"
        elif fro[0] > to[0]:
            return "r"
        elif fro[1] > to[1]:
            return "d"
        elif fro[0] < to[0]:
            return "l"