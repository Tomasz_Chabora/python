def sum_seq(sequence):
    result = 0
    
    for element in sequence:
        if isinstance(element, (list, tuple)):
            result += sum_seq(element)
        else:
            result += element
        
    return result

#----------------

sequence = [1,(2,3),[],[4,(5,6,7)],8,[9],[(1,[1,([([([(10)])],1)]), 2])]]

print("sequence = " + str(sequence))
print("sum = " + str(sum_seq(sequence)))