def flatten(sequence):
    result = []
    
    for element in sequence:
        if isinstance(element, (list, tuple)):
            result += flatten(element)
        else:
            result.append(element)
        
    return result

#----------------

sequence = [1,(2,3),[],[4,(5,6,7)],8,[9],[(1,[1,([([([(10)])],1)]), 2])]]

print("sequence = " + str(sequence))
print("flatten(sequence) = " + str(flatten(sequence)))