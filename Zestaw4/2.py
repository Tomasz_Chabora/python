def ruler(length):
    upper = ""
    lower = ""

    for i in range(length):
        upper += "|"
        lower += str(i)
        
        for ii in range(4):
            upper += "."
            if ii < 5 - len(str(i + 1)): lower += " "

    upper += "|"
    lower += str(length)

    return "\n".join([upper, lower])

def grid(width, height):
    grid = ""

    for y in range(height):
        for x in range(width):
            grid += "+---"
        
        grid += "+\n"
        
        for x in range(width):
            grid += "|   "
            
        grid += "|\n"

    for x in range(width):
        grid += "+---"
    
    grid += "+"

    return grid

#------------------------------------

while True:
    length = raw_input("Enter ruler length: ")
    try:
        length = int(length)
        break
    except:
        print("Invalid input")
        
while True:
    width = raw_input("Enter grid width: ")
    try:
        width = int(width)
        break
    except:
        print("Invalid input")
        
while True:
    height = raw_input("Enter grid height: ")
    try:
        height = int(height)
        break
    except:
        print("Invalid input")

print(ruler(length))
print(grid(width, height))