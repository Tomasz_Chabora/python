def factorial(n):
    result = 1
    
    for i in range(n):
        result *= (i+1)
    
    return result

#----------------

while True:
    n = raw_input("Enter number: ")
    try:
        n = int(n)
        break
    except:
        print("Invalid input")

print(str(n) + "! = " + str(factorial(n)))