def fibonacci(n):
    if n<2:
        return n
    
    result = 0
    n_1 = 1
    n_2 = 0
    
    for i in range(n-1):
        result = n_1 + n_2
        n_2 = n_1
        n_1 = result
    
    return result

#----------------

while True:
    n = raw_input("Enter number: ")
    try:
        n = int(n)
        break
    except:
        print("Invalid input")

print("fibonacci(" + str(n) + ") = " + str(fibonacci(n)))