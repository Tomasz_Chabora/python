def P(i, j, first = True):
    if (first): P.mem = {}
    
    if i == 0 and j == 0:
        return 0.5
    elif i == 0 and j > 0:
        return 1.0
    elif j == 0 and i > 0:
        return 0
    
    if (i,j) not in P.mem:
        P.mem[(i,j)] = 0.5 * (P(i-1, j, False) + P(i, j-1, False))
    
    return P.mem[(i,j)]

i = int(raw_input("Enter i: "))
j = int(raw_input("Enter j: "))
print(P(i, j))