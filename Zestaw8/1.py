def gcd(n1, n2):
    while n2 != 0:
        n3 = n1 % n2
        n1 = n2
        n2 = n3
    
    return n1

def solve1(a, b, c):
    if a == 0 and b == 0:
        if c == 0:
            print("Solution is pair (p, q), where p,q is any real number")
        else:
            print("No solutions")
        return
    
    if a != 0 and b != 0 and c % gcd(a, b) != 0:
        print("No solutions")
        return
    
    if a == 0:
        print("Solution is pair (r, {}), where r is any real number".format(-c/b))
        return
    
    if b == 0:
        print("Solution is pair ({}, r), where r is any real number".format(-c/a))
        return
    
    print("Solution is pair (r, {}r {:+}), where r is any real number".format(-a/b, -c/b))

print("Equation ax + by + c = 0")
a = float(raw_input("Enter a: "))
b = float(raw_input("Enter b: "))
c = float(raw_input("Enter c: "))

solve1(a, b, c)