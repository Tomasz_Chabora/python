def P(i, j):
    mem = {}
    if i == 0 and j == 0: return 0.5
    elif i == 0: return 1
    elif j == 0: return 0
    
    for ii in range(i+1):
        for jj in range(j+1):
            if ii == 0 and jj == 0:
                mem[(ii, jj)] = 0.5
            elif ii == 0:
                mem[(ii, jj)] = 1.0
            elif jj == 0:
                mem[(ii, jj)] = 0
            else:
                mem[(ii, jj)] = 0.5 * (mem[(ii-1, jj)] + mem[(ii, jj-1)])
    
    return mem[(i,j)]

i = int(raw_input("Enter i: "))
j = int(raw_input("Enter j: "))
print(P(i, j))