import math;

def heron(a, b, c):
    if not (a < b+c and b < a+c and c < a+b):
        raise ValueError("Triangle inequality not met")
    
    p = 0.5 * (a+b+c)
    
    return math.sqrt(p * (p-a) * (p-b) * (p-c))

a = int(raw_input("Enter side a: "))
b = int(raw_input("Enter side b: "))
c = int(raw_input("Enter side c: "))

print("Surface equals: " + str(heron(a,b,c)))