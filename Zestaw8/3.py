from random import random;
from math import pi;

def calc_pi(n=100):
    k = 0
    
    for i in range(n):
        x = random()
        y = random()
        
        if pow(x, 2) + pow(y, 2) <= 1:
            k += 1
    
    print(4 * float(k) / n)

n = int(raw_input("Enter number of points: "))

print("\/ calculated pi")
calc_pi(n)
print(str(pi) + "\n/\\ real pi")