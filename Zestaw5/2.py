import unittest
from fracs import *

class TestFractions(unittest.TestCase):
    def setUp(self): pass

    def test_add_frac(self):
        self.assertEqual(add_frac([1, 3], [1, 3]), [2, 3])
        self.assertEqual(add_frac([1, 2], [1, 3]), [5, 6])

    def test_sub_frac(self):
        self.assertEqual(sub_frac([2, 3], [1, 3]), [1, 3])
        self.assertEqual(sub_frac([1, 2], [1, 3]), [1, 6])

    def test_mul_frac(self):
        self.assertEqual(mul_frac([1, 2], [2, 2]), [2, 4])
        self.assertEqual(mul_frac([1, 2], [3, 4]), [3, 8])

    def test_div_frac(self):
        self.assertEqual(div_frac([1, 2], [2, 1]), [1, 4])
        self.assertEqual(div_frac([1, 2], [3, 4]), [4, 6])

    def test_is_positive(self):
        self.assertTrue(is_positive([1,2]))
        self.assertTrue(is_positive([-1,-2]))
        self.assertFalse(is_positive([-1,2]))
        self.assertFalse(is_positive([1,-2]))

    def test_is_zero(self):
        self.assertTrue(is_zero([0,1]))
        self.assertTrue(is_zero([0,2]))
        self.assertTrue(is_zero([0,-3]))

    def test_cmp_frac(self):
        self.assertEqual(cmp_frac([1, 3], [2, 3]), -1)
        self.assertEqual(cmp_frac([1, 4], [1, 3]), -1)
        self.assertEqual(cmp_frac([1, 2], [1, 2]), 0)
        self.assertEqual(cmp_frac([1, 2], [2, 4]), 0)
        self.assertEqual(cmp_frac([2, 3], [1, 3]), 1)
        self.assertEqual(cmp_frac([1, 3], [1, 4]), 1)

    def test_frac2float(self):
        self.assertEqual(frac2float([1, 10]), 0.1)
        self.assertEqual(frac2float([1, 4]), 0.25)
        self.assertEqual(frac2float([1, 2]), 0.5)
        self.assertEqual(frac2float([0, 1]), 0)
        self.assertEqual(frac2float([0, 2]), 0)
        self.assertEqual(frac2float([1, 1]), 1)
        self.assertEqual(frac2float([2, 2]), 1)
        self.assertEqual(frac2float([2, 1]), 2)
        self.assertEqual(frac2float([8, 2]), 4)
        self.assertEqual(frac2float([10, 4]), 2.5)

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()