def add_frac(frac1, frac2):
    if frac1[1] != frac2[1]:
        frac1[0] *= frac2[1]
        frac2[0] *= frac1[1]
        frac1[1] = frac2[1] = frac1[1] * frac2[1]
    
    return [frac1[0] + frac2[0], frac1[1]]

def sub_frac(frac1, frac2):
    if frac1[1] != frac2[1]:
        frac1[0] *= frac2[1]
        frac2[0] *= frac1[1]
        frac1[1] = frac2[1] = frac1[1] * frac2[1]
    
    return [frac1[0] - frac2[0], frac1[1]]

def mul_frac(frac1, frac2):
    return [frac1[0] * frac2[0], frac1[1] * frac2[1]]

def div_frac(frac1, frac2):
    return [frac1[0] * frac2[1], frac1[1] * frac2[0]]

def is_positive(frac):
    return (frac[0] < 0) == (frac[1] < 0)

def is_zero(frac):
    return frac[0] == 0

def cmp_frac(frac1, frac2):
    f1 = frac2float(frac1)
    f2 = frac2float(frac2)
    
    if f1 == f2:
        return 0
    elif f1 < f2:
        return -1
    else:
        return 1

def frac2float(frac):
    return float(frac[0]) / float(frac[1])